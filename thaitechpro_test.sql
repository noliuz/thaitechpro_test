-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 10, 2020 at 10:02 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thaitechpro_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `formulas`
--

CREATE TABLE `formulas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `formulas`
--

INSERT INTO `formulas` (`id`, `recipe_id`, `ingredient_id`, `amount`, `created_at`, `updated_at`) VALUES
(3, 1, 3, 1.00, NULL, NULL),
(5, 2, 3, 1.50, NULL, NULL),
(8, 1, 6, 10.00, '2020-02-09 00:36:29', '2020-02-09 00:36:29'),
(9, 1, 4, 10.00, '2020-02-09 00:56:26', '2020-02-09 00:56:26'),
(10, 1, 7, 1.00, '2020-02-09 00:56:38', '2020-02-09 00:56:38'),
(11, 2, 5, 1.00, '2020-02-09 00:56:49', '2020-02-09 00:56:49'),
(13, 3, 1, 300.00, '2020-02-09 01:12:04', '2020-02-09 01:12:04'),
(14, 3, 7, 1.00, '2020-02-09 01:12:10', '2020-02-09 01:12:10'),
(15, 3, 6, 1.00, '2020-02-09 01:12:15', '2020-02-09 01:12:15'),
(16, 3, 5, 2.00, '2020-02-09 01:12:19', '2020-02-09 01:12:19'),
(17, 2, 8, 400.00, '2020-02-09 01:14:26', '2020-02-09 01:14:26'),
(18, 1, 8, 100.00, '2020-02-09 01:14:50', '2020-02-09 01:14:50'),
(19, 2, 2, 5.00, '2020-02-09 02:59:59', '2020-02-09 02:59:59'),
(20, 4, 9, 100.00, '2020-02-09 03:01:42', '2020-02-09 03:01:42'),
(21, 4, 4, 5.00, '2020-02-09 03:01:48', '2020-02-09 03:01:48'),
(22, 4, 6, 3.00, '2020-02-09 03:01:58', '2020-02-09 03:01:58'),
(23, 5, 10, 300.00, '2020-02-09 03:13:17', '2020-02-09 03:13:17'),
(24, 5, 7, 50.00, '2020-02-09 03:13:24', '2020-02-09 03:13:24'),
(25, 5, 6, 10.00, '2020-02-09 03:13:31', '2020-02-09 03:13:31');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Pork', 'g', '2020-02-08 04:00:13', '2020-02-08 04:00:13'),
(2, 'Carrot', 'g', '2020-02-08 04:00:49', '2020-02-08 04:00:49'),
(3, 'Lemon', 'g', '2020-02-08 04:00:56', '2020-02-08 04:00:56'),
(4, 'Chilli', 'g', '2020-02-08 04:01:00', '2020-02-08 04:01:00'),
(5, 'Pepper', 'g', '2020-02-08 04:01:17', '2020-02-08 04:01:17'),
(6, 'Sugar', 'g', '2020-02-08 04:01:20', '2020-02-08 04:01:20'),
(7, 'Salt', 'g', '2020-02-08 04:01:27', '2020-02-08 04:01:27'),
(8, 'Rice', 'g', '2020-02-09 01:13:48', '2020-02-09 01:13:48'),
(9, 'Red Fish', 'g', '2020-02-09 03:01:11', '2020-02-09 03:01:11'),
(10, 'Cat', 'g', '2020-02-09 03:13:06', '2020-02-09 03:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2020_02_05_100446_create_recipes_table', 1),
(16, '2020_02_05_100649_create_ingredients_table', 1),
(17, '2020_02_05_101420_create_formulas_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Boiled Rice', NULL, NULL),
(3, 'Fired Pork', '2020-02-09 01:11:47', '2020-02-09 01:11:47'),
(4, 'Chilli Red Fish', '2020-02-09 03:01:30', '2020-02-09 03:01:30'),
(5, 'Fired Salt Cat', '2020-02-09 03:12:54', '2020-02-09 03:12:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `formulas`
--
ALTER TABLE `formulas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `formulas`
--
ALTER TABLE `formulas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
