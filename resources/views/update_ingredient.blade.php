@extends('tmp')
@section('content')
  <h2>Edit Ingredient</h2>
  <form method="post" action="/ingredient/update">
    {{ csrf_field() }}
    <font>Name</font> <input type="text" name="name" value="{{$ingre->name}}"><br>
    <font>Unit</font> <input type="text" name="unit" value="{{$ingre->unit}}"><br><br>
    <input type="submit" value="Submit">
    <input type="hidden" name="id" value="{{$ingre->id}}">
  </form>

@stop
