@extends('tmp')
@section('content')
  <h2>Show Formulas</h2>

  <select id="recipe_combo" onchange="showById(this)">
    @foreach ($recipe_list as $rep)
      <option value={{$rep->id}}
        @if($rep->id == $recipe_id)
          selected
        @endif
      >
        {{$rep->name}}
      </option>
    @endforeach
  </select>

  <table>
  @foreach ($ingres as $ingre)
    <tr>
      <td>
        {{$ingre->name}} {{$ingre->amount}} {{$ingre->unit}}<br>
      </td>
      <td>
        <img src="/images/x.png" width=30 height=30 onclick="deleteIngre({{$ingre->id}})">
      </td>
    </tr>
  @endforeach
  </table>

  <h2>Add</h2>
  <form method="post" action="/formula/create">
    {{ csrf_field() }}
    Ingredient <select id="ingre_combo" onchange="changeIngresID()">
      @foreach ($ingres_all as $ingre_all)
        <option value={{$ingre_all->id}}>
          {{$ingre_all->name}}
        </option>
      @endforeach
    </select><br>
    <table>
      <tr>
        <td>Amount</td>
        <td><input type="text" name="amount"></td>
        <td> <div id='unit'>{{$ingres_all[0]->unit}}</unit></td>
      </tr>
    </table><br>
    <input type=hidden name='recipe_id' value='{{$recipe_id}}'>
    <input type=hidden id='ingredient_id' name='ingredient_id' value='{{$ingres_all[0]->id}}'>
    <input type="submit" value="Add">
  </form>

  <script>
    function changeIngresID() {
      document.getElementById('ingredient_id').value = document.getElementById("ingre_combo").value;
    }

    function deleteIngre(ingre_id) {
      //alert("/formula/delete/{{$recipe_id}}/"+ingre_id);
      window.location = "/formula/delete/{{$recipe_id}}/"+ingre_id;
    }

    function showById(selObj) {
      var value = selObj.value;
      //alert(value);
      window.location = "/menu/show/"+value;
    }

  </script>

@stop
