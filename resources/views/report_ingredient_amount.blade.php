@extends('tmp')
@section('content')
  <h2>Ingredients amount monthly report</h2>
  @foreach ($res as $r)
    {{$r['name']}} {{number_format($r['amount'],2)}} g<br>
  @endforeach

@stop
