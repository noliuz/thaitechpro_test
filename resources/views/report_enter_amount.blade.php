@extends('tmp')
@section('content')
  <h2>Enter recipe amount for monthly ingredients calculation</h2>

  <form method="post" action="/report/totalIngredient">
    {{ csrf_field() }}
    <table>
    @foreach ($recipes as $recipe)
      <tr>
        <td>
          {{$recipe->name}}
        </td>
        <td>
          <input type="text" name="{{$recipe->id}}"> <font color='grey'>dishes</font>
        </td>
      </tr>
    @endforeach
    </table>
    <input type="submit" value="Submit">

  </form

  <script>

  </script>

@stop
