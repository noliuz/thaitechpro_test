@extends('tmp')
@section('content')
  <h2>Show Recipe</h2>

  <table>
  @foreach ($recipes as $recipe)
      <tr>
        <td>
          {{ $recipe->name }}
        </td>
        <td>
          <img src="/images/edit.png" onclick="editRecipe({{$recipe->id}})">  <img src="/images/x.png" width=30 height=30 onclick="deleteRecipe({{$recipe->id}})">
        </td>
      </tr>
  @endforeach
  </table>

  <script>
    function deleteRecipe(id) {
      window.location = "/recipe/delete/"+id;
    }

    function editRecipe(id) {
      window.location = "/recipe/edit/"+id;
    }

  </script>

@stop
