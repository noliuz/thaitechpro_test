<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Chief Management</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css">
  </head>

  <body>
  <h1><font color="#0000e7">Chief Management System</font></h1>

  <div class="dropdown">
    <button class="dropbtn">Recipe</button>
    <div class="dropdown-content">
      <a href="/recipe/show">Show</a>
      <a href="/recipe">Create</a>
    </div>
  </div>

  <div class="dropdown">
    <button class="dropbtn">Formula</button>
    <div class="dropdown-content">
      <a href="/menu/show/-1">Show</a>
    </div>
  </div>

  <div class="dropdown">
    <button class="dropbtn">Ingredient</button>
    <div class="dropdown-content">
      <a href="/ingredient/show">Show</a>
      <a href="/ingredient">Create</a>
    </div>
  </div>

  <div class="dropdown">
    <button class="dropbtn">Report</button>
    <div class="dropdown-content">
      <a href="/report/showForm">Amount Form</a>
    </div>
  </div>

  <hr width=300 align='left'>

  @yield('content')
  </body>
</html>
