@extends('tmp')
@section('content')
  <h2>Show Ingredient</h2>

  <table>
  @foreach ($ingres as $ingre)
      <tr>
        <td>
          {{ $ingre->name }} ({{ $ingre->unit }})
        </td>
        <td>
          <img src="/images/edit.png" onclick="editIngre({{$ingre->id}})">
          <!--<img src="/images/x.png" width=30 height=30 onclick="deleteIngre({{$ingre->id}})"> -->
        </td>
      </tr>
  @endforeach
  </table>

  <script>
    function deleteIngre(id) {
      window.location = "/ingredient/delete/"+id;
    }

    function editIngre(id) {
      window.location = "/ingredient/edit/"+id;
    }

  </script>

@stop
