@extends('tmp')
@section('content')
  <h2>Edit Recipe</h2>
  <form method="post" action="/recipe/update">
    {{ csrf_field() }}
    <font>Name</font> <input type="text" name="name" value="{{$recipe->name}}"><br><br><br>
    <input type="submit" value="Submit">
    <input type="hidden" name="id" value="{{$recipe->id}}">
  </form>

@stop
