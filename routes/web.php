<?php

use Illuminate\Http\Request;
use App\Ingredient;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ingredient', function () {
  return view('main_ingredient');
});
Route::get('/ingredient/show','IngredientController@show');
Route::post('/ingredient/update','IngredientController@update');
Route::get('/ingredient/edit/{id}','IngredientController@edit');
Route::get('/ingredient/delete/{id}','IngredientController@delete');
Route::post('/ingredient/create', 'IngredientController@create');
Route::get('/ingredient/getUnit/{id}', 'IngredientController@getUnit');

Route::get('/recipe', function () {
  return view('recipe_create');
});
Route::post('/recipe/create', 'RecipeController@create');
Route::get('/recipe/show', 'RecipeController@show');
Route::get('/recipe/delete/{id}','RecipeController@delete');
Route::get('/recipe/edit/{id}','RecipeController@edit');
Route::post('/recipe/update', 'RecipeController@update');

Route::get('/menu/show/{recipe_id}','FormulaController@show');
Route::post('/formula/create','FormulaController@create');
Route::get('/formula/delete/{rep_id}/{ingre_id}','FormulaController@delete');

Route::get('/report/showForm','ReportController@show_form');
Route::post('/report/totalIngredient','ReportController@tg_report');
