<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;


class RecipeController extends Controller
{
    public function create(Request $req) {
      if (empty($req->name)) {
        return 'Please fill all fields';
      }
      //check duplicate name
      $count = Recipe::where('name', $req->name)->count();
      if ($count != 0) {
        return "This name is used.";
      } else {
        $recipe = new Recipe;
        $recipe->name = $req->name;
        $recipe->save();
        return redirect('/recipe');
      }
    }

    public function update(Request $req) {
      $recipe = Recipe::find($req->id);
      $recipe->name = $req->name;
      $recipe->save();

      return redirect('/recipe/show');
    }

    public function delete($id) {
      $recipe = Recipe::find($id);
      $recipe->delete();

      return redirect('/recipe/show');
    }

    public function show() {
      $recipes = Recipe::all();
      return view('recipe_show')->with('recipes',$recipes);
    }

    public function edit($id) {
      $recipe = Recipe::find($id);
      return view('recipe_edit')->with('recipe',$recipe);
    }
}
