<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;


class IngredientController extends Controller
{
    public function show() {
      $ingres = Ingredient::all();
      return view('show_ingredient')->with('ingres',$ingres);
    }

    public function edit($id) {
      $ingre = Ingredient::find($id);
      return view('update_ingredient')->with('ingre',$ingre);
    }

    public function getUnit($id) {
      $ingre = Ingredient::find($id);
      return $ingre->unit;
    }

    public function update(Request $req) {
      //save
      $ingre = Ingredient::find($req->id);
      $ingre->name = $req->name;
      $ingre->unit = $req->unit;
      $ingre->save();

      //show
      $ingres = Ingredient::all();
      return view('show_ingredient')->with('ingres',$ingres);
    }

    public function delete($id) {
      $ingre = Ingredient::find($id);
      $ingre->delete();
      return redirect('/ingredient/show');
    }

    public function create(Request $req) {
      //check empty
      if (empty($req->name) || empty($req->unit)) {
        return 'Please fill all fields';
      }
      //check duplicate name
      $count = Ingredient::where('name', $req->name)->count();
      if ($count != 0) {
        return "This name is used.";
      } else {
        $ing = new ingredient;
        $ing->name = $req->name;
        $ing->unit = $req->unit;
        $ing->save();

        return redirect('/ingredient');
      }
    }
}
