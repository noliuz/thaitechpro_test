<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Ingredient;
use App\Formula;

class ReportController extends Controller
{
    public function show_form() {
      $recipes = Recipe::orderBy('name')->get();
      return view('report_enter_amount')->with('recipes',$recipes);
    }

    public function tg_report(Request $req) {
      $recipes = Recipe::orderBy('name')->get();
      $recipe_count = Recipe::count();

      //prepare ingredient array
      $total_ingres = [];
      $all_ingres = Ingredient::all();
      foreach ($all_ingres as $all_ingre) {
        $total_ingres[$all_ingre->id] = 0;
      }

      //cal
      foreach($recipes as $rep) {
        $amount_dish = $req[$rep->id];
        $recipe_id = $rep->id;
        //get all ingredient ids of this recipe
        $ingres = Formula::where('recipe_id',$recipe_id)->get();
        foreach ($ingres as $ingre) {
          $total_ingres[$ingre->ingredient_id] += $ingre->amount*$amount_dish;
        }
      }

      //convert to json
      $res = [];
      foreach ($total_ingres as $ingre_id => $amount) {
        //find ingre name
        $ingre_name = '';
        foreach ($all_ingres as $all_ingre) {
          if ($all_ingre->id == $ingre_id) {
            $ingre_name = $all_ingre->name;
            break;
          }
        }
        $one_ingre['name'] = $ingre_name;
        $one_ingre['amount'] = $amount;
        array_push($res,$one_ingre);
      }

      //return $res;
      //return $all_ingres;
      //return json_encode($res);
      return view('report_ingredient_amount',['res'=>$res]);
    }
}
