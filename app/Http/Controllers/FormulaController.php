<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Formula;
use App\Ingredient;
use App\Recipe;

class FormulaController extends Controller
{
    public function show($recipe_id) {
      if ($recipe_id == -1) {
        $res = Recipe::min('id');
        $recipe_id = $res;
      }
      //recipes name list
      $recipe_list = Recipe::all();

      $data = [];
      $ingre_list = [];
      //get recipe name
      $recipe = Recipe::find($recipe_id);
      //get formula details
      $formulas = Formula::where('recipe_id', $recipe_id)->get();
      //get all ingredient
      $ingres_all = Ingredient::orderBy('name')->get();

      foreach($formulas as $formula) {
        //get ingredient details
        $ingre_id = $formula->ingredient_id;
        $ingre = Ingredient::where('id',$ingre_id)->get();
        $ingre[0]->amount = number_format((float)$formula->amount, 2, '.', '');
        array_push($ingre_list,$ingre[0]);
      }

      return view('formula_show', ['name' => $recipe->name,'ingres'=>$ingre_list,'recipe_list' => $recipe_list,'recipe_id' => $recipe_id,'ingres_all' => $ingres_all]);
      //return view('formula_show')->with('data',$data);
    }

    public function create(Request $req) {
      $formula = new Formula;
      $formula->recipe_id = $req->recipe_id;
      $formula->ingredient_id = $req->ingredient_id;
      $formula->amount = $req->amount;
      $formula->save();

      return redirect('/menu/show/'.$req->recipe_id);
    }

    public function delete($rep_id,$ingre_id) {
      $formula = Formula::where('recipe_id',$rep_id)
                        ->where('ingredient_id',$ingre_id);
      $formula->delete();

      return redirect('/menu/show/'.$rep_id);
    }
}
